/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SopaBinaria;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class PruebaPrevio {
    public static void main(String[] args) throws IOException, Exception {
        // :)
        Scanner leer = new Scanner(System.in);
        SopaBinaria s = new SopaBinaria("src/Datos/binarios.xls");
        System.out.println("Original:\n"+s.toString());
        String sopaOrdenada = s.getMatrizOrdenada().toString();
        System.out.println("Ordenada:\n"+sopaOrdenada);
        
        System.out.println("¿Desea imprimir la sopa binaria ordenada en pdf? responda si o no");
        if("si".equals(leer.nextLine())){
            s.getMatrizOrdenada().getInformePdf();
            System.out.println("Creando archivo...Listo.\nPrograma finalizado.");
        } else {
            System.out.println("Programa finalizado.");
        }
    }
    
}
