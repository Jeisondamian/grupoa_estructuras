/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IBinario;
import Modelo.Bit;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author madar
 */
public class SopaBinaria implements IBinario{
    
    private Bit mySopaBinaria[][];

    public SopaBinaria() {
    }
    
     public SopaBinaria(String rutaArchivoExcel) throws FileNotFoundException, IOException {
         HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(rutaArchivoExcel));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        this.mySopaBinaria=new Bit[canFilas][];
        for (int i = 0; i < canFilas; i++) {
            HSSFRow filas = hoja.getRow(i);
            int cantCol=filas.getLastCellNum();
            this.mySopaBinaria[i]=new Bit[cantCol];
             
            for(int j=0;j<cantCol;j++) {
                
                boolean valor = Integer.parseInt(filas.getCell(j).getStringCellValue()) == 1;
                this.mySopaBinaria[i][j]= new Bit(valor);
            }
        }
    }

    @Override
    public SopaBinaria getMatrizOrdenada() {
        SopaBinaria ordenada = new SopaBinaria();
        int columna, fila;
        int contador = 0, paridad=0;
        int posicion[] = new int[mySopaBinaria[0].length];
        
        for(columna=0; columna<mySopaBinaria[0].length; columna++){
            for(fila=0; fila<mySopaBinaria.length; fila++){
                if(mySopaBinaria[fila][columna].isValor()){
                    contador++;
                }else{
                    if(contador>0 && contador%2==0) paridad++;
                    contador = 0;
                }
            }
            posicion[columna] = paridad;
            paridad = 0;
        }
        ordenada.setMySopaBinaria(ordenar(posicion, this.getMySopaBinaria()));
        return ordenada;
    }
    
    public Bit[][] ordenar(int vector[], Bit[][] matriz){
        int posicion = 0, mayor = 0;
        Bit ordenada[][] = new Bit[matriz.length][matriz[0].length];
        
        for(int columna=0; columna<ordenada[0].length; columna++){
            for(int i=0; i<vector.length; i++){
                if(vector[i]>mayor){
                    mayor = vector[i];
                    posicion = i;
                }
            }
        
            for(int fila=0; fila<ordenada.length; fila++){
                ordenada[fila][columna] = matriz[fila][posicion];
            }
            
            vector[posicion] = 0;
            mayor = 0;
            posicion = 0;
        }
        return ordenada;
    }
    
    public String toString(){
        String msg = "";
        for(int i=0; i<this.mySopaBinaria.length; i++){
            for(int j=0; j<this.mySopaBinaria[i].length; j++){
                if(this.mySopaBinaria[i][j].isValor())
                    msg+= 1+" ";
                else
                    msg+= 0+" ";
            }
            msg+="\n";
        }
        return msg;
    }
    
    public void getInformePdf() throws Exception {
        
        Document documento = new Document();
        FileOutputStream ficheroPdf = new FileOutputStream("src/Datos/informe.pdf");
        PdfWriter.getInstance(documento,ficheroPdf);
        
        documento.open();
        PdfPTable tabla = new PdfPTable(mySopaBinaria[0].length);
        for(int i=0; i<mySopaBinaria.length; i++){
            for(int j=0; j<mySopaBinaria[0].length; j++){
                PdfPCell celda = new PdfPCell();
                if(mySopaBinaria[i][j].isValor()){
                    celda = new PdfPCell(new Phrase(1+""));
                }else{
                    celda = new PdfPCell(new Phrase(0+""));
                }
                tabla.addCell(celda);
            }
        }
        documento.add(tabla);
        documento.close();
    }

    public void setMySopaBinaria(Bit[][] mySopaBinaria) {
        this.mySopaBinaria = mySopaBinaria;
    }

    public Bit[][] getMySopaBinaria() {
        return mySopaBinaria;
    }
    
}
